from django.db import models
from django.contrib.auth.models import User

from decimal import Decimal

# Create your models here.


class Plane(models.Model):
    tail_number = models.CharField(max_length=64, primary_key=True)  # "N43337"
    name = models.CharField(max_length=64)  # "Warrior II"
    manufacturer = models.CharField(max_length=64)  # Piper
    model = models.CharField(max_length=64)  # PA28-151
    engine_count = models.IntegerField(default=1)

    def __str__(self):
        return self.tail_number


class Airport(models.Model):
    icao = models.CharField(max_length=4, primary_key=True)
    link = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.icao


class Flight(models.Model):
    flight_date = models.DateField()  # "flight_date": "2019-08-31",

    plane = models.ForeignKey(Plane, on_delete=models.PROTECT)
    airport_depart = models.ForeignKey(
        Airport, on_delete=models.PROTECT, related_name="%(class)s_from"
    )  # "from": "KEEN",
    airport_arrive = models.ForeignKey(
        Airport, on_delete=models.PROTECT, related_name="%(class)s_to"
    )  # "to": "KEEN",

    user = models.ForeignKey(User, on_delete=models.PROTECT)

    instructor = models.CharField(
        max_length=64, blank=True
    )  # "instructor": "Gene Moody",
    remarks = models.TextField(
        blank=True
    )  # "remarks": "normal to/l, shortfield to/l, std turns, constant speed, constant descents",

    landings = models.IntegerField()

    airplane_sel_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    airplane_mel_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    cross_country_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    day_time = models.DecimalField(max_digits=2, decimal_places=1, default=Decimal(0.0))
    night_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    actual_instrument_time = models.DecimalField(
        decimal_places=1, max_digits=2, default=Decimal(0.0)
    )
    simulated_instrument_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    ground_trainer_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    dual_received_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    pilot_in_command_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )
    total_time = models.DecimalField(
        max_digits=2, decimal_places=1, default=Decimal(0.0)
    )

    link = models.CharField(max_length=200, blank=True)

    leg = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.flight_date}: {self.plane} {self.total_time}h - {self.airport_depart} -> {self.airport_arrive}"
