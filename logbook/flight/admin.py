from django.contrib import admin

from flight.models import Flight, Plane, Airport

# Register your models here.


class FlightAdmin(admin.ModelAdmin):
    pass


admin.site.register(Flight, FlightAdmin)


class PlaneAdmin(admin.ModelAdmin):
    pass


admin.site.register(Plane, PlaneAdmin)


class AirportAdmin(admin.ModelAdmin):
    pass


admin.site.register(Airport, AirportAdmin)
